## Ansible

### Prerequisites

In order to run ansible it needs to be installed on the executing host, to do so see [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installation-guide).
The target host needs to have python installed on it.

### Usage

To run a playbook use following command:
```
ansible-playbook -i <inventory_file> --key-file=<path_to_private_key_for_remote_authentication> <playbook_name>
```
Example:
```
ansible-playbook -i ansible_inventory --key-file=~/.ssh/some_id_rsa install_nginx_docker.yml
```

### Testing

Ansible is usually not unintentionally harmful. Tasks are executed and if there is nothing to do (package already installed, key already added, etc.), the task will complete without making changes on the host (status ok instead of changed).
This is controlled using the state property for each task, when not specified the task will always be marked as leaving the host "changed".
Due to this behaviour you can add task for task to your playbook and rerun it over and over again without harming the remote host.

If you want to have some peace of mind you may use the docker image in the .test folder. It will start a docker container running Ubuntu 19.04 with a running SSH daemon.
You just need to add your public key to it and you can specify said host in the ansible_inventory.

**Note**: Starting services may not always work within a docker container due to the fact that systemd is not running with a PID of 1.
Starting nginx for example works like a charm, however docker will not come up (but also not throw an error during apt install, so the playbook runs seemingly successfully).

Copying your public key to the docker container:
```
docker cp ~/.ssh/id_rsa.pub slc_ansible_ubuntu_1:/root/.ssh/authorized_keys
```
Entry in ansible_inventory (resp. the port mapped to docker port 22):
```
127.0.0.1:44402
```