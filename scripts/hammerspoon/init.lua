-------------------------------------------------------------------
-- Globals
-------------------------------------------------------------------
hs.window.animationDuration = 0

-------------------------------------------------------------------
-- Window Layouts
-------------------------------------------------------------------

-- These are just convenient names for layouts. We can use numbers
-- between 0 and 1 for defining 'percentages' of screen real estate
-- so 'right30' is the window on the right of the screen where the
-- vertical split (x-axis) starts at 70% of the screen from the
-- left, and is 30% wide.
--
-- And so on...
units = {
    right50 = { x = 0.50, y = 0.00, w = 0.50, h = 1.00 },
    right30 = { x = 0.70, y = 0.00, w = 0.30, h = 1.00 },
    right70 = { x = 0.30, y = 0.00, w = 0.70, h = 1.00 },
    left50 = { x = 0.00, y = 0.00, w = 0.50, h = 1.00 },
    left70 = { x = 0.00, y = 0.00, w = 0.70, h = 1.00 },
    left30 = { x = 0.00, y = 0.00, w = 0.30, h = 1.00 },
    top50 = { x = 0.00, y = 0.00, w = 1.00, h = 0.50 },
    bot50 = { x = 0.00, y = 0.50, w = 1.00, h = 0.50 },
}

local inrange = function(from, to, value)
    return math.min(math.max(from, value), to)
end

local resize = function(amount)
    local screenRect = hs.screen.mainScreen():frame()
    local window = hs.window.focusedWindow()
    local rect = window:frame()
    local delta = amount / 100;
    window:move(hs.geometry.rect(
        inrange(0, 1, 1 / screenRect.w * (rect.x - screenRect.x) + delta * -0.5),
        inrange(0, 1, 1 / screenRect.h * (rect.y - screenRect.y) + delta * -0.5),
        inrange(0, 1, 1 / screenRect.w * rect.w + delta),
        inrange(0, 1, 1 / screenRect.h * rect.h + delta)
    ), nil, true)
end

-- All of the mappings for moving the window of the 'current' application to the right spot.
modifier = { 'cmd', 'alt', 'ctrl' }
hs.hotkey.bind(modifier, 'Up', function() hs.window.focusedWindow():move(units.top50, nil, true) end)
hs.hotkey.bind(modifier, 'Left', function() hs.window.focusedWindow():move(units.left50, nil, true) end)
hs.hotkey.bind(modifier, 'Right', function() hs.window.focusedWindow():move(units.right50, nil, true) end)
hs.hotkey.bind(modifier, 'Down', function() hs.window.focusedWindow():move(units.bot50, nil, true) end)
hs.hotkey.bind(modifier, ']', function() hs.window.focusedWindow():move(units.right30, nil, true) end)
hs.hotkey.bind(modifier, '0', function() hs.window.focusedWindow():move(units.right70, nil, true) end)
hs.hotkey.bind(modifier, '[', function() hs.window.focusedWindow():move(units.left30, nil, true) end)
hs.hotkey.bind(modifier, '9', function() hs.window.focusedWindow():move(units.left70, nil, true) end)
hs.hotkey.bind(modifier, 'm', function() hs.window.focusedWindow():maximize() end)
hs.hotkey.bind(modifier, '-', function() resize(-10) end)
hs.hotkey.bind(modifier, '=', function() resize(10) end)
hs.hotkey.bind(modifier, 'n', function() hs.window.focusedWindow():moveToScreen(hs.screen.mainScreen():next(), nil, true) end)
hs.hotkey.bind(modifier, 'p', function() hs.window.focusedWindow():moveToScreen(hs.screen.mainScreen():previous(), nil, true) end)